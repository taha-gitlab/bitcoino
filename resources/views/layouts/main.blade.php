<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Bitcoino</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="/css/main.css">
        <!-- JavaScript -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/autonumeric@4.5.4"></script>

    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="content">
                <div class="title m-b-md">
                    @yield("content")
                </div>

            </div>
        </div>
        <script>
            new AutoNumeric('.myInput', { currencySymbol : '' ,alwaysAllowDecimalCharacter:false,decimalPlaces:5});
            new AutoNumeric('#read-only-input', { currencySymbol : '' });
            // calcPrice();
            var $enabledInput = $("#enableInput");
            var $readOnlyInput = $("#read-only-input")
            var $coinPrice    = $("#coin-chart tr:nth-child(1) td:nth-child(4)").text().match(/([0-9\.])/g).join('');
            var coinTo = true;
            $coinTo1 = 9703;
            $coinTo2 = 9703;
            $("#coin").on("change",function () {
                $symbol = $("#fiat").val();
                $.ajax({
                    type:'POST',
                    url:'/getPrice',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id": $(this).val()
                    },
                    success:function(data) {
                        $coinPrice = data["quote"][$symbol]["price"].toFixed(3);
                    }
                });
            })


            $("#fiat").on("change",function () {
                $symbol = $(this).val();
                $coinSym = $("#coin option:selected").text();
                console.log($coinSym);
                $.ajax({
                    type:'POST',
                    url:'/changeCurrency',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "currencySymbol": $(this).val()
                    },
                    success:function(data) {
                        console.log(data);
                        $("#coin-chart").empty();
                        console.log(data);
                        $.each(data, function (i, item) {
                            if(item["name"] == $coinSym){
                                $coinPrice = item["quote"][$symbol]["price"];
                            }
                            $a = item["quote"][$symbol]["percent_change_1h"]+'';
                            console.log($a);
                            $td = ($a.toLowerCase().indexOf("-") >= 0) ? $('<td>')
                                .text(addCommas((item["quote"][$symbol]["percent_change_1h"] * 10)
                                    .toFixed(2))+'%')
                                .addClass("border px-4 py-2 text-red-500") : $('<td>')
                                .text(addCommas((item["quote"][$symbol]["percent_change_1h"] * 10)
                                    .toFixed(2))+'%').addClass("border px-4 py-2 text-green-500");

                            var $tr = $('<tr>').append(
                                $('<td>').html(`<img src="${item["logo"]}" class="mx-auto w-10" alt="Cryptocurrency">`).addClass("border px-4 py-2"),
                                $('<td>').text(item["name"]).addClass("border px-4 py-2"),
                                $td,
                                $('<td>').text(addCommas(item["quote"][$symbol]["price"].toFixed(2))).addClass("border px-4 py-2"),
                                $('<td>').text(addCommas(item["quote"][$symbol]["volume_24h"].toFixed(0))).addClass("border px-4 py-2"),
                                $('<td>').text(addCommas(item["quote"][$symbol]["market_cap"].toFixed(0))).addClass("border px-4 py-2"),
                            ); //.appendTo('#records_table');
                            console.log($tr.wrap('<p>').appendTo("#coin-chart"));
                        });
                    }
                });
            })

            $("#coinButton").on("click", function () {
                $a = $("#ToCoin1 option:selected").val();
                $b = $("#ToCoin2 option:selected").val();
                $sum = $a + "," + $b;
                $.ajax({
                    type:'POST',
                    url:'/getPrices',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id": $sum
                    },
                    success:function(data) {
                        $coinTo1 = data[$b]['quote']['USD']["price"];
                        $coinTo2 = data[$a]['quote']['USD']["price"];
                        // CoinToCoin($coinTo1, $coinTo2);
                        CoinToCoin($coinTo1,$coinTo2,$("#CoinToCoin1").val().match(/([0-9\.])/g).join(''));

                    }
                });
            });

            function CoinToCoin(coin1 , coin2, number) {

                $("#CoinToCoin2").val((number * coin2 / coin1).toFixed(3));
                console.log(coin1);
            }

            function addCommas(nStr)
            {
                nStr += '';
                x = nStr.split('.');
                x1 = x[0];
                x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1)) {
                    x1 = x1.replace(rgx, '$1' + ',' + '$2');
                }
                return x1 + x2;
            }
            $enabledInput.on("keyup", function (text) {
                showPrice($(this).val().match(/([0-9\.])/g).join(''), coinTo);
            })

            function showPrice(number, coinTo){
                // console.log("Price: "+ number * )
                if(coinTo == true) {
                    $readOnlyInput.val((number / $coinPrice).toFixed(3));
                    console.log($coinPrice);
                }else{
                    $readOnlyInput.val((number * $coinPrice).toFixed(3));
                    console.log($coinPrice);
                }
            }



            $("#change").on("click", function () {
                if($("#label2").text() == ":فیات"){
                    $("#label2").text(":ارزدیجیتال");
                    $("#label1").text(":فیات");
                    console.log("a");
                }else if($("#label2").text() == ":ارزدیجیتال"){
                    $("#label2").text(":فیات");
                    $("#label1").text(":ارزدیجیتال");
                }
                coinTo = !coinTo;
                $readOnlyInput.val();
                $enabledInput.val();

            })
        </script>
    </body>
</html>
