@extends("layouts.main")


@section('content')
    <div class="container mx-auto h-screen flex-col">
        <div class="input-container">
            <div class="flex mx-auto justify-center items-center rounded border-gray-300 mt-10 h-64 relative">
                <div class="flex mx-auto w-full justify-center items-center ">
                    <div class="mx-1 flex-row">
                        <div class="relative">
                            <div class="pointer-events-none absolute inset-y-0 left-0 flex items-center px-4 text-gray-700">
                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                            </div>
                            <select id="coin" class="appearance-none bg-gray-200 border border-gray-200 text-gray-700 py-4 pl-10 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                                @foreach($cryptos as $crypto)
                                    <option value="{{ $crypto["id"] }}">{{ $crypto["name"] }}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>
                    <input
                        readonly
                        id="read-only-input"
                        class=" bg-gray-400 text-right w-2/5 rounded py-4 px-10 text-gray-700 leading-tight focus:outline-none "
                        placeholder="مقدار" type="text">
                    <label for="read-only-input" id="label1" class="flex flex-col font-semibold text-2xl mx-2">:ارزدیجیتال</label>

                    <div class="mx-2">
                        <div class="relative">
                            <div class="pointer-events-none absolute inset-y-0 left-0 flex items-center px-2 text-gray-700">
                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                            </div>
                            <select id="fiat" class="appearance-none bg-gray-200 border border-gray-200 text-gray-700 py-4 px-4 pl-10 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                                <option value="USD">USD $</option>
                                <option value="AUD">AUD $</option>
                                <option value="BRL">BRL R$</option>
                                <option value="CAD">CAD $</option>
                                <option value="CHF">CHF Fr</option>
                                <option value="CLP">CLP $</option>
                                <option value="CNY">CNY ¥</option>
                                <option value="CZK">CZK Kč</option>
                                <option value="DKK">DKK kr</option>
                                <option value="EUR">EUR €</option>
                                <option value="GBP">GBP £</option>
                                <option value="HKD">HKD $</option>
                                <option value="HUF">HUF Ft</option>
                                <option value="IDR">IDR Rp</option>
                                <option value="ILS">ILS ₪</option>
                                <option value="INR">INR ₹</option>
                                <option value="JPY">JPY ¥</option>
                                <option value="KRW">KRW ₩</option>
                                <option value="MXN">MXN $</option>
                                <option value="MYR">MYR RM</option>
                                <option value="NOK">NOK kr</option>
                                <option value="NZD">NZD $</option>
                                <option value="PHP">PHP ₱</option>
                                <option value="PKR">PKR ₨</option>
                                <option value="PLN">PLN zł</option>
                                <option value="RUB">RUB ₽</option>
                                <option value="SEK">SEK kr</option>
                                <option value="SGD">SGD S$</option>
                                <option value="THB">THB ฿</option>
                                <option value="TRY">TRY ₺</option>
                                <option value="TWD">TWD NT$</option>
                                <option value="ZAR">ZAR R</option>
                                <option value="AED">AED د.إ</option>
                                <option value="BGN">BGN лв</option>
                                <option value="HRK">HRK kn</option>
                                <option value="MUR">MUR ₨</option>
                                <option value="RON">RON lei</option>
                                <option value="ISK">ISK kr</option>
                                <option value="NGN">NGN ₦</option>
                                <option value="COP">COP $</option>
                                <option value="ARS">ARS $</option>
                                <option value="PEN">PEN S/.</option>
                                <option value="VND">VND ₫</option>
                                <option value="UAH">UAH ₴</option>
                                <option value="BOB">BOB Bs.</option>
                                <option value="ALL">ALL L</option>
                                <option value="AMD">AMD ֏</option>
                                <option value="AZN">AZN ₼</option>
                                <option value="BAM">BAM KM</option>
                                <option value="BDT">BDT ৳</option>
                                <option value="BHD">BHD .د.ب</option>
                                <option value="BMD">BMD $</option>
                                <option value="BYN">BYN Br</option>
                                <option value="CRC">CRC ₡</option>
                                <option value="CUP">CUP $</option>
                                <option value="DOP">DOP $</option>
                                <option value="DZD">DZD د.ج</option>
                                <option value="EGP">EGP £</option>
                                <option value="GEL">GEL ₾</option>
                                <option value="GHS">GHS ₵</option>
                                <option value="GTQ">GTQ Q</option>
                                <option value="HNL">HNL L</option>
                                <option value="IQD">IQD ع.د</option>
                                <option value="IRR">IRR ﷼</option>
                                <option value="JMD">JMD $</option>
                                <option value="JOD">JOD د.ا</option>
                                <option value="KES">KES Sh</option>
                                <option value="KGS">KGS с</option>
                                <option value="KHR">KHR ៛</option>
                                <option value="KWD">KWD د.ك</option>
                                <option value="KZT">KZT ₸</option>
                                <option value="LBP">LBP ل.ل</option>
                                <option value="LKR">LKR Rs</option>
                                <option value="MAD">MAD د.م.</option>
                                <option value="MDL">MDL L</option>
                                <option value="MKD">MKD ден</option>
                                <option value="MMK">MMK Ks</option>
                                <option value="MNT">MNT ₮</option>
                                <option value="NAD">NAD $</option>
                                <option value="NIO">NIO C$</option>
                                <option value="NPR">NPR ₨</option>
                                <option value="OMR">OMR ر.ع.</option>
                                <option value="PAB">PAB B/.</option>
                                <option value="QAR">QAR ر.ق</option>
                                <option value="RSD">RSD дин.</option>
                                <option value="SAR">SAR ر.س</option>
                                <option value="SSP">SSP £</option>
                                <option value="TND">TND د.ت</option>
                                <option value="TTD">TTD $</option>
                                <option value="UGX">UGX Sh</option>
                                <option value="UYU">UYU $</option>
                                <option value="UZS">UZS so&#039;m</option>
                                <option value="VES">VES Bs.</option>
                            </select>

                        </div>
                    </div>
                    <button id="change" class="btn bg-orange-500 px-8 py-3 left-auto right-auto text-white absolute top-0 right-0 rounded mt-12 mr-20 font-semibold">تغییر</button>
                    <input
                        id="enableInput"
                        class="shadow text-right w-2/5 myInput appearance-none border rounded py-4 px-10 text-gray-700 leading-tight focus:outline-none "
                        placeholder="مقدار" type="text">
                </div>
                <label for="read-only-input" id="label2" class="flex flex-col font-semibold text-2xl mx-2">:فیات</label>

            </div>



{{--            Coin To Coin --}}
            <div class="container mx-auto h-screen flex-col">
                <div class="input-container">
                    <div class="flex mx-auto justify-center items-center rounded border-gray-300 mt-10 h-64 relative">
                        <div class="flex mx-auto w-full justify-center items-center ">
                            <div class="mx-1 flex-row">
                                <div class="relative">
                                    <div class="pointer-events-none absolute inset-y-0 left-0 flex items-center px-4 text-gray-700">
                                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                                    </div>
                                    <select id="ToCoin2" class="appearance-none bg-gray-200 border border-gray-200 text-gray-700 py-4 pl-10 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                                        @foreach($cryptos as $crypto)
                                            <option value="{{ $crypto["id"] }}">{{ $crypto["name"] }}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                            <input
                                readonly
                                id="CoinToCoin2"
                                class=" bg-gray-400 text-right w-2/5 rounded py-4 px-10 text-gray-700 leading-tight focus:outline-none "
                                placeholder="مقدار" type="text">

                            <div class="mx-2">
                                <div class="relative">
                                    <div class="pointer-events-none absolute inset-y-0 left-0 flex items-center px-2 text-gray-700">
                                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                                    </div>
                                    <select id="ToCoin1" class="appearance-none bg-gray-200 border border-gray-200 text-gray-700 py-4 pl-10 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                                        @foreach($cryptos as $crypto)
                                            <option value="{{ $crypto["id"] }}">{{ $crypto["name"] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <button id="coinButton" class="p-5 bg-green-400">submit</button>
                            <input
                                id="CoinToCoin1"
                                class="shadow text-right w-2/5 myInput appearance-none border rounded py-4 px-10 text-gray-700 leading-tight focus:outline-none "
                                placeholder="مقدار" type="text">
                        </div>

                    </div>
{{--            End Coin To Coin--}}




        </div>
        <div class="mx-auto flex flex-col items-center h-full ">
            <table class="table-auto mt-10 w-full" style="direction: rtl">
                <thead>
                <tr>
                    <th class="py-2"></th>
                    <th class="py-2">رمزارز</th>
                    <th class="py-2">تغییر</th>
                    <th class="py-2">قیمت</th>
                    <th class="py-2">حجم ۲۴ ساعت بازار</th>
                    <th class="py-2">حجم کل بازار</th>

                </tr>
                </thead>
                <tbody id="coin-chart" class="justify-center mt-2" style="text-align: center;direction: ltr">
                @foreach($cryptos as $crypto)
                    <tr>
                        <td class="border px-4 py-2"><img class="w-10 m-auto" src="{{ $images[$crypto["id"]]['logo'] }}" alt="{{ $crypto["name"] }}"></td>
                        <td class="border px-4 py-2">{{ $crypto["name"] }}</td>
                        @if(Illuminate\Support\Str::contains(number_format($crypto["quote"]["USD"]["percent_change_1h"] * 10, 2), "-"))
                            <td class="border px-4 py-2 text-red-500" style="direction: ltr">{{ number_format($crypto["quote"]["USD"]["percent_change_1h"] * 10, 2) }}%</td>
                        @else
                            <td class="border px-4 py-2 text-green-500" style="direction: ltr">+{{ number_format($crypto["quote"]["USD"]["percent_change_1h"] * 10, 2) }}%</td>
                        @endif
                        <td class="border px-4 py-2">{{ number_format($crypto["quote"]["USD"]["price"], 2) }}$</td>
                        <td class="border px-4 py-2">{{ number_format($crypto["quote"]["USD"]["volume_24h"], 0) }}$</td>
                        <td class="border px-4 py-2">{{ number_format($crypto["quote"]["USD"]["market_cap"], 0) }}$</td>
                    </tr>
                @endforeach
                <img src="" alt="">
                </tbody>
            </table>


        </div>

    </div>

    <script >

    </script>
@endsection
