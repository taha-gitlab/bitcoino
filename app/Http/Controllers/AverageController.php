<?php

namespace App\Http\Controllers;

use App\Total;
use DB;
use App\Average;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class AverageController extends Controller
{
    protected $baseDomain;

    protected $API_KEY;

    public function __construct()
    {
        $this->baseDomain = config("services.crypto.domain");
        $this->API_KEY = config("services.crypto.key");
    }

    public function putPrice()
    {
        $db = new Average;
        $cryptos = Http::get("{$this->baseDomain}cryptocurrency/listings/latest?CMC_PRO_API_KEY={$this->API_KEY}&limit=15")
            ->json()["data"];
        foreach ($cryptos as $crypto){
            Average::create([
                'crypto' => $crypto["name"],
                'price' => $crypto["quote"]["USD"]["price"],
            ]);
        }
    }

    public function putAverage(){
        $avrages = DB::select("SELECT crypto, AVG(price) as average FROM averages Group By crypto");
        foreach ($avrages as $average) {
            Total::create([
                'crypto' => $average->crypto,
                'avg' => $average->average,
            ]);
            Average::truncate();
        }
    }
}
