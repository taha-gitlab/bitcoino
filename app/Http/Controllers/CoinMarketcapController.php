<?php

namespace App\Http\Controllers;

use App\Http\interfaces\CoinMarketInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


class CoinMarketcapController extends Controller implements CoinMarketInterface
{
    protected $baseDomain;

    protected $API_KEY;

    public function __construct()
    {
        $this->baseDomain = config("services.crypto.domain");
        $this->API_KEY = config("services.crypto.key");
    }


    public function index()
    {
        $cryptos = $this->getCrypto();

        $images = $this->getImages();

        return view("index", [
            "cryptos" => $cryptos,
            "images" => $images
        ]);
    }

    public function getPrice($id = "Bitcoin"){
//        price of a bitcoin
        $id = strtolower(\request("id"));

        return Http::get("{$this->baseDomain}cryptocurrency/quotes/latest?CMC_PRO_API_KEY={$this->API_KEY}&id=".$id)
            ->json()["data"][$id];
    }

    public function getPrices($id = "Bircoin"){
        $id = strtolower(\request("id"));

        return Http::get("{$this->baseDomain}cryptocurrency/quotes/latest?CMC_PRO_API_KEY={$this->API_KEY}&id=".$id)
            ->json()["data"];
    }

    public function changeCurrency(){
        $currencySymbol = request("currencySymbol");

        $cryptos = Http::get("{$this->baseDomain}cryptocurrency/listings/latest?CMC_PRO_API_KEY={$this->API_KEY}&limit=15&convert=".$currencySymbol)
            ->json()["data"];

        $images = $this->getImages();

        $result = [];
        foreach ($cryptos as $crypto){
            $crypto["logo"] = $images[$crypto["id"]]["logo"];
            $result[] = $crypto;
        }

         return $result;
    }

    public function getImages()
    {
        $cryptos = $this->getCrypto();

        $imageId = [];
        foreach ($cryptos as $crypto){
            $imageId[] += $crypto['id'];
        }

        $imageId = implode(",", $imageId);

        return Http::get("{$this->baseDomain}cryptocurrency/info?CMC_PRO_API_KEY={$this->API_KEY}&id={$imageId}")
            ->json()["data"];
    }

    public function getCrypto(){
        return Http::get("{$this->baseDomain}cryptocurrency/listings/latest?CMC_PRO_API_KEY={$this->API_KEY}&limit=15")
            ->json()["data"];
    }


}
