<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "CoinMarketcapController@index");
Route::post("/getPrice", "CoinMarketcapController@getPrice");
Route::post("/getPrices", "CoinMarketcapController@getPrices");
Route::post("/changeCurrency", "CoinMarketcapController@changeCurrency");

Route::get("/putPrice", "AverageController@putPrice");
Route::get("/test", "AverageController@putAverage");
